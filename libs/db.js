var mongoose = require('mongoose');
var Users = (function(mongoose){
	var Schema = mongoose.Schema,
	    userSchema,
	    User;
	mongoose.connect('mongodb://localhost/nicknames')

	userSchema = new Schema({
    name: {type: String, unique: true},
    created_at: { type: Date, default: Date.now }
		});
	User = mongoose.model('User', userSchema, 'people');

  return {
    users: function(req, res, next) {
      User.find({}, function(err, data) {
        if(err) { return next(err); }
        res.send(data)
      });
    },
    user: function(req, res, next) {
      User.findOne({name: req.body.username}, function(err, data) {
        if(err) { return next(err); }
        res.send(data)
      });
    },
    create: function(req, res, next) {
      new User({
        name: req.body.username
      }).save(function(err, data) {
        if(err) {
          err["message"] = "Username already Taken";
          res.send(500, err.message);
        }
        res.send(200,'OK')
      });
    }
  }
})(mongoose);

exports.User = Users;
