(function (global, factory) {
  factory(global);
})(window, function(window) {
  var jLite = function(selector) {
    return new jLite.fn.init(selector);
  };
  jLite.fn = jLite.prototype = {
    selector: '',
    cssProps: '',
    html: function(data) {
      if(data)
        this[0].innerHTML = data;
      else
        return this[0].innerHTML;
    },

    val: function(data) {
      if(data)
        this[0].value = data;
      else
        return this[0].value;
    },

    each : function(obj, cb, args) {
      var value,
          i = 0,
          length = obj.length;

      if(length && length > 0) {
        for( ;i < length; i++) {
          value = args ? cb.apply(obj[i], args) : cb.apply(obj[i])

          if(value === false)
            break;
        }
      } else {
				for ( i in obj ) {
          value = args ? cb.apply(obj[i], args) : cb.apply(obj[i])

					if (value === false) {
						break;
					}
				}
      }
    },

    append: function(data) {
      this.html(this.html() + data)
    },

    on: function(events, cb) {
      this.each(this, function() {
        jLite.event.add(this, events, cb)
      });
    },

    css: function(prop, value) {
      if(value)
          this.cssProps.setProperty(prop, value)

      if(window.getComputedStyle) {
        return getComputedStyle(this[0]).getPropertyValue(prop);
      } else {
        return this[0].currentStyle[prop]
      }
    },

    attr: function(key, value) {
      if(value)
        this[0].key = value;
      return this[0].getAttribute(key)
    },

    data: function(attr, value) {
      var datasets = this[0].datasets;
      if(value) {
        if(datasets)
          datasets[attr] = value;
        else
          this[0].setAttribute("data-" + attr, value);
      }
      if(datasets) {
        return datasets[attr];
      } else {
        return this.attr("data-" + attr)
      }
    },

    toggle: function() {
      if(this.css("display") == "none") {
        this.css("display", this.data("toggle") || "block");
      }
      else if(this.css("display") != "none") {
        this.data("toggle", this.css("display"));
        console.log(this.prevProp)
        this.css("display", "none")
      }
    }
  };

  jLite.fn.init = function(sel, elem) {
    this.selector = sel;

    if(elem = document.querySelectorAll(sel)) {
      this.cssProps = elem[0].style;

      if(elem.length == 1)
        this[0] = elem[0];
      else
        this[0] = [].slice.call(elem);
      return this;
    }
  };

  jLite.event = {
    add: function(elem, types, fn) {
      var i = 0;
      types = types.split(" ");
      while(type = types[i++]) {
        if(elem.addEventListener) {
          elem.addEventListener(type, fn, false)
        }
      }
    }
  };

  jLite.fn.init.prototype = jLite.fn;

  window._$ = window.jLite = jLite;
  return jLite;
});
