var chatApp = angular.module("chatApp", ['ngRoute']);

chatApp.config(function($routeProvider, $locationProvider) {
  $locationProvider.html5Mode(true);
  $routeProvider.
    when('/', {
      templateUrl: 'partials/index',
      controller: 'RegistrationsController'
    }).
    when('/users', {
      templateUrl: 'partials/users',
      controller: 'UsersController'
    }).
    when('/chats', {
      templateUrl: 'partials/chats',
      controller: 'ChatsController'
    }).
    when('/500', {
      templateUrl: 'errors/500'
    }).
    otherwise({ redirectTo: '/'});
});

chatApp.controller("RegistrationsController", function($http, $location, $scope) {
  $scope.formData = {};
  $scope.createUser = function() {
    $http.post('/register', $scope.formData).success(function() {
      $location.path('/chats');
    }).error(function(data, status) {
      $scope.error = { status: status, message: data }
      console.log(data, status);
    });
  }
});

chatApp.controller("UsersController", function($http, $location, $scope) {
  $scope.users = {};
  $http.get('/users').success(function(data) {
    $scope.users.nicknames = data;
  }).error(function() {
    $location.path('/500')
  })
});

chatApp.controller("ChatsController", function($http, $location, $scope) {
});
