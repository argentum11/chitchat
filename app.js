var express = require('express'),
		app = express(),
    sass = require('node-sass'),
    sassMiddleware = require('node-sass-middleware'),
		server = require('http').Server(app),
    bodyParser = require('body-parser'),
    io = require('socket.io')(server),
    db = require('./libs/db.js'),
    nicknames = [];

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json())

app.set('port', 8000)
app.set('view engine', 'jade');

app.use(
  sassMiddleware({
    src: __dirname + '/sass',
    dest: __dirname + '/public',
    debug: true
  })
);
app.use(express.static(__dirname + '/public'));

app.get('/', function(req, res) {
  res.render('layout')
});

app.post('/register', db.User.create);

app.get('/partials/:name', function(req, res) {
  res.render('partials/' + req.params.name);
})

io.sockets.on('connection', function (socket) {
  console.log("a user connected")
	socket.emit('news', {message: 'hello brat'})
  socket.on('send_message', function(data) {
    io.sockets.emit('new_message', { message: data.reply })
  });
});

app.use(function (err, req, res, next) {
  console.log(err)
  res.status(500);
  res.render('500', { error: err.message })
});

server.listen(app.get('port'), function() {
  console.log("Listening on port : " + app.get('port'))
})
